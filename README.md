## Assumptions

- Input & Output date format: CSV, which can be easily used together with excel for data entry

  - Input

    - date format: ISO 8601 YYYY-MM-DD, localization & date format validation is skipped for simple
    - only caculate monthly payslip, other situations such as payment based on hourly rate is out of scrope for this exercise
    - other data fileds' format validation is ignored for simple

    <br/>

    | id  | firstName | lastName | annualSalary | superRate | startDate  |
    | --- | --------- | -------- | ------------ | --------- | ---------- |
    | 1   | leslie    | su       | 120000       | 0.09      | 2018-06-01 |

  - Output

    | id  | name      | period          | grossIncome | incomeTax | netIncome | superannuation |
    | --- | --------- | --------------- | ----------- | --------- | --------- | -------------- |
    | 1   | leslie su | 01 Jun - 30 Jun | 10000       | 2669      | 7331      | 900            |

* Leaves

  leaves is not considered for payment calculation

* Unit Test
  - unit tests is value for code quality, but choice & implementention of unit test framework doesn't matter for this project
  - test coverage rate is ignored

## Usage

1. install npm and node if you haven't yet, my node version is v10.9.0, but should works on node 6+
2. clone or fork this project
3. run following commands

   ```
   cd payslip && npm i

   node index.js
   ```
