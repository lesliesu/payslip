const assert = require('assert');
const getPayslipByMonth = require('./payslipService');

const superRate = 0.09;
const startDateStr = '2018-05-01';
assert.deepEqual(
  getPayslipByMonth({ annualSalary: 18200, superRate, startDateStr }),
  {
    grossIncome: 1516, incomeTax: 0, netIncome: 1516, superannuation: 136,
  },
);

assert.deepEqual(
  getPayslipByMonth({ annualSalary: 37000, superRate, startDateStr }),
  {
    grossIncome: 3083, incomeTax: 298, netIncome: 2785, superannuation: 277,
  },
);
