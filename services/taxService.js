const moment = require('moment');

const incomeTax2018 = {
  start: moment('2017-07-01'),
  end: moment('2018-06-30'),
  brackets: [
    {
      bottom: 0,
      top: 18200,
      base: 0,
      rate: 0,
    },
    {
      bottom: 18201,
      top: 37000,
      base: 0,
      rate: 0.19,
    },
    {
      bottom: 37001,
      top: 87000,
      base: 3572,
      rate: 0.325,
    },
    {
      bottom: 87001,
      top: 180000,
      base: 19822,
      rate: 0.37,
    },
    {
      bottom: 180001,
      top: Infinity,
      base: 54232,
      rate: 0.45,
    },
  ],
};

const incomeTax2017 = {
  start: moment('2016-07-01'),
  end: moment('2017-06-30'),
  brackets: [
    {
      bottom: 0,
      top: 18200,
      base: 0,
      rate: 0,
    },
    {
      bottom: 18201,
      top: 35000,
      base: 0,
      rate: 0.19,
    },
    {
      bottom: 35001,
      top: 70000,
      base: 3572,
      rate: 0.325,
    },
    {
      bottom: 70001,
      top: 180000,
      base: 19822,
      rate: 0.37,
    },
    {
      bottom: 180001,
      top: Infinity,
      base: 54232,
      rate: 0.45,
    },
  ],
};

const taxTable = [incomeTax2018, incomeTax2017];

const getMonthlyIncomeTax = (startDate, annualSalary) => {
  const endDate = startDate.clone().endOf('month');

  // find tax year related income tax entity
  const incomeTax = taxTable.find(
    tax => tax.start.isSameOrBefore(startDate, 'day') && tax.end.isSameOrAfter(endDate, 'day'),
  );
  if (!incomeTax) {
    console.warn(`unsupported tax year / month: ${startDate}`);
    return undefined;
  }

  // find tax bracket
  const taxBracket = incomeTax.brackets.find(
    bracket => annualSalary >= bracket.bottom && annualSalary <= bracket.top,
  );

  return Math.round((taxBracket.base + (annualSalary - taxBracket.bottom) * taxBracket.rate) / 12);
};

module.exports = {
  taxTable,
  getMonthlyIncomeTax,
};
