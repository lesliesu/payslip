const moment = require('moment');

const { getMonthlyIncomeTax } = require('./taxService');

const getPayslipByMonth = ({ annualSalary, superRate, startDateStr }) => {
  const startDate = moment(startDateStr);

  const grossIncome = Math.floor(annualSalary / 12);
  const incomeTax = getMonthlyIncomeTax(startDate, annualSalary);
  let netIncome;
  let superannuation;
  if (incomeTax >= 0) {
    // TODO calculate payment based on leaves (assume there is a LeaveService)
    netIncome = grossIncome - incomeTax;
    superannuation = Math.round(grossIncome * superRate);
  }

  const endDate = startDate.clone().endOf('month');

  const period = `${startDate.format('DD MMM')} - ${endDate.format('DD MMM')}`;

  return {
    period,
    grossIncome,
    incomeTax,
    netIncome,
    superannuation,
  };
};

// TODO
// const getPayslipByHour = ({
//   hourlyRate, superRate, startDateStr, hours,
// }) => {};

module.exports = getPayslipByMonth;
