const assert = require('assert');
const moment = require('moment');

const { getMonthlyIncomeTax } = require('./taxService');

const month = moment('2018-06-01');
assert(getMonthlyIncomeTax(month, 0) === 0, 'annual salary 0: tax 0');
assert(getMonthlyIncomeTax(month, 18200) === 0, 'annual salary 18200: tax 0');
assert(
  getMonthlyIncomeTax(month, 37000) === Math.round(3572 / 12),
  `annual salary 37000: tax ${Math.round(3572 / 12)}`,
);
assert(
  getMonthlyIncomeTax(month, 87000) === Math.round(19822 / 12),
  `annual salary 87000: tax ${Math.round(19822 / 12)}`,
);
assert(
  getMonthlyIncomeTax(month, 180000) === Math.round(54232 / 12),
  `annual salary 180000: tax ${Math.round(54232 / 12)}`,
);

const month2 = moment('2015-06-01');
assert(
  getMonthlyIncomeTax(month2, 0) === undefined,
  'unsupported tax year / month',
);
