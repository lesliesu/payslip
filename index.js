const csv = require('csv-parser');
const write = require('csv-write-stream');
const fs = require('fs');
const path = require('path');

const getPayslipByMonth = require('./services/payslipService');

const writer = write();
writer.pipe(fs.createWriteStream(path.join(__dirname, './data/output.csv')));

const transform = ({
  id,
  firstName,
  lastName,
  annualSalary,
  superRate,
  startDate,
}) => {
  const payslip = getPayslipByMonth({
    annualSalary,
    superRate,
    startDateStr: startDate,
  });
  // console.log(payslip);
  const name = `${firstName}  ${lastName}`;
  writer.write({
    id, name, ...payslip,
  });
};

fs.createReadStream(path.join(__dirname, './data/input.csv'))
  .pipe(csv())
  .on('data', transform)
  .on('end', () => {
    writer.end();
  });
